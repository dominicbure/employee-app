@component('mail::message')
# Introduction
Dear {{ $email_content['name'] }}

{{ $email_content['title'] }}

{{ $email_content['body'] }}

@component('mail::button', ['url' => ''])
Button Text
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
