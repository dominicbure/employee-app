<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserStoreRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Models\Department;
use App\Models\Employee;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Exception;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $is_admin = $this->verifyAdminPermission(auth()->user()->id);
        //dd($request->all());

        if($is_admin) {
            try {
                $users = User::all();

                if ($request->has('search')) {
                    try {
                        $users = User::where('first_ame', 'like', "%{$request->search}%")
                        ->orWhere('middle_name', 'like', "%{$request->search}%")
                        ->orWhere('last_name', 'like', "%{$request->search}%")
                        ->orWhere('email', 'like', "%{$request->search}%")
                        ->get();
                    } catch(Exception $e) {
                        Log::error($e);
                        return redirect()->route('home')->with('message', 'no records were found');
                    }

                    return view('users.index', compact('users'));
                }
                return view('users.index', compact('users'));
            } catch(Exception $e) {
                Log::error($e);
                return view('users.index')->with('message', 'no records were found');
            }
        } else {
            $users = (new User)->getUserById(auth()->user()->id);
            
            return view('users.index', compact('users'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments = Department::all();

        return view('users.create', compact('departments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserStoreRequest $request)
    {

        $userInputData = [
            'first_name' => $request->first_name,
            'middle_name' => $request->middle_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'is_admin' => $request->is_admin,
            'password' => Hash::make($request->password),
        ];
      
        try {
            $user = (new User())->storeData($userInputData);
        } catch (Exception $e) {
            Log::error($e);
        }

        try {
            $employeeInputData = [
                'address' => $request->address,
                'department_id' => $request->department,
                'birthdate' => $request->birthdate,
                'date_hired' => $request->date_hired,
                'user_id' => $user->id,
            ];
    
            try {
                $employee = (new Employee())->storeData($employeeInputData);
            } catch (Exception $e) {
                Log::error($e);

                return redirect()->route('users.index')->with('message', 'Employee record not created. Please Check logs');
            }

        } catch(Exception $e) {
            Log::error($e);

            return redirect()->route('users.index')->with('message', 'User not created. Please Check logs');
        }
        return redirect()->route('users.index')->with('message', 'User Register Succesfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $users = (new User)->getUserById($id);

        } catch (Exception $e) {
            Log::error($e);
            return redirect()->route('users.index')->with('error', 'user does not exist');
        }

        if (!$users) {
            Log::error($e);

            return redirect()->route('users.index')->with('error', 'Admin profile does not exist');
        }
       
        return view('users.show', compact('users'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        try {
            $user = User::with('employee')->find($user->id);
        } catch (Exception $e) {
            Log::error($e);
            return redirect()->route('users.index')->with('error', 'failed to find User for update');
        }

        try {
            $departments = Department::all();
        } catch (Exception $e) {
            Log::error($e);
            return redirect()->route('users.index')->with('error', 'no departments found');
        }

        return view('users.edit', compact('user', 'departments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, User $user)
    {   
        try {
            $user->update([
                'first_name' => $request->first_name,
                'middle_name' => $request->middle_name,
                'last_name' => $request->last_name,
                'email' => $request->email,
                'is_admin' => $request->is_admin ?? 0,
            ]);
        } catch (Exception $e) {
            Log::error($e);

            return redirect()->route('users.index')->with('message', 'User record not updated successfully');
        }
        
        if($user) {
            try {
                $user->employee->update([
                    'address' => $request->address,
                    'department_id' => $request->department,
                    'birthdate' => $request->birthdate,
                    'date_hired' => $request->date_hired,
                ]);
            } catch (Exception $eh) {
                Log::error($e);

                return redirect()->route('users.index')->with('message', 'Employee record not updated successfully');
            }
        }
        

        return redirect()->route('users.index')->with('message', 'User Updated Succesfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        if (auth()->user()->id == $user->id) {
            return redirect()->route('users.index')->with('message', 'You are deleting yourself.');
        }
        try {
            $user = User::with('employee')->find($user->id);
            $user->delete();
        } catch (Exception $e) {
            Log::error($e);

            return redirect()->route('users.index')->with('message', 'user record not deleted successfully');
        }
        
        return redirect()->route('users.index')->with('message', 'User Deleted Succesfully');
    }
}
