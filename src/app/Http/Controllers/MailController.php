<?php

namespace App\Http\Controllers;

use App\Mail\GenericMail;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Exception;
use Illuminate\Support\Facades\Log;
 
class MailController extends Controller
{
   public function sendBulkMail(Request $request)
   {    
        try {
            $users = User::get(['email', 'first_name']);
        } catch (Exception $e) {
            Log::error($e);
            return redirect()->route('home')->with('message', 'No users were found');
        }
        if($request->title && $request->body) {
            $email_content = [
                'title' => $request->title,
                'body' => $request->body
            ];
        } else {
            return redirect()->route('home')->with('message', 'Title or body is empty');
        }

        foreach ( $users as $recipient) {
            $email_content['name'] = $recipient['first_name'];

            Mail::to( $recipient['email'])->send(new GenericMail($email_content));
        }


        return redirect()->route('home')->with('message', 'Email successfully fired off!');
   }

    public function create() 
    {
        return view('send-emails.create');
    }

   
}