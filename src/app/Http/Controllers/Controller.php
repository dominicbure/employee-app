<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Check if user is admin and if not, may the user access the User
     *
     * @param  mixed $userId
     * @return void
     */
    public function verifyAdminPermission()
    {
        if (Auth::user()->is_admin != 1) {
            return false;
        } else {
            return true;
        }
    }

    public function verifyUserPermission(int $userId)
    {
        $user = Auth::user();
        if (Auth::user()->user_is_admin != 1) {
            if ($userId != Auth::id()) {
                return redirect()->route('users.create')->with('message', 'You are forbidden to view this pagessss.');
            } else {
                return view('users.show', compact('user'));
            }
        } else {
            return true;
        }
    }
}
