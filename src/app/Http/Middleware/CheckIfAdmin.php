<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckIfAdmin
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @return void
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->is_admin == 1) {
            return $next($request);
        }

        return redirect()->route('users.index')->with('error', 'You are forbidden to view this page.');
    }
}
