<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Exception;
Use Illuminate\Support\Facades\Log;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'first_name',
        'middle_name',
        'is_admin',
        'last_name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'is_admin',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        
        'birthdate' => 'datetime:Y-m-d',
    
        'date_hired' => 'datetime:Y-m-d',
    ];

    public function employee()
    {
        return $this->hasOne(Employee::class);
    }

     /**
    * Store data
    */
    public function storeData($data)
    {
        $user = null;

        try {
            $user = User::create($data);
        } catch (Exception $e) {
            Log::error($e);
        }

        return $user;
    }

    /**
     * Get user by ID without stokvel, trader details
     */
    public function getUserById($id)
    {
        $user = null;

        try {
            $user = User::where('id', '=', $id)
                ->get();
        } catch (Exception $e) {
            Log::error($e);
        }

        return $user;
    }
}
