<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Exception;
use Illuminate\Support\Facades\Log;

class Employee extends Model
{
    use HasFactory;

    protected $fillable = [
        'first_name',
        'last_name',
        'middle_name',
        'address',
        'department_id',
        'user_id',
        'birthdate',
        'date_hired',
    ];

    protected $casts = [
    'birthdate' => 'datetime:Y-m-d',

    'date_hired' => 'datetime:Y-m-d',
];

    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function storeData($data)
    {
        $employee = null;

        try {
            $employee = Employee::create($data);
        } catch (Exception $e) {
            Log::error($e);
        }

        return $employee;
    }
}