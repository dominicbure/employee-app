<?php

use App\Http\Controllers\Admin\ChangePasswordController;
use App\Http\Controllers\Admin\DepartmentController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\MailController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::resource('departments', DepartmentController::class);

Route::middleware(['auth'])->group(function () {
    Route::resource('users', UserController::class);
    Route::post('users/{user}/change-password', [ChangePasswordController::class, 'ChangePassword'])->name('users.change.password');

    Route::middleware(['admin'])->group(function () {
        Route::resource('users', UserController::class)->only(['create']);
    });

    Route::post('/send-mail', [MailController::class, 'sendBulkMail'])->name('send.mail');

    Route::get('/create-email', [MailController::class, 'create'])->name('send.create');

    Route::get('{any}', function () {
        return view('employees.index');
    })->where('any', '.*');


});



