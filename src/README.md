Technologies:
Laravel 8
PHP 7+
Nodejs latest version
Nginx
Vuejs(I was unable to complete implementation)
Docker

Steps to reproduce to get the App running:

1. git clone https://dominicbure@bitbucket.org/dominicbure/employee-app.git

2. Cd into project

3. If you have docker installed you can run the following command in the root of the source code:
   docker-compose build

4. Run docker-compose up -d

5. If you are on Mac you can docker desktop to help access the containers else you can run the following command to access the PHP container
   docker exec -it employee-app_php_1 bash

6. Once in container run this command: composer install

7. Copy the .env.example to .env. The file is setup for docker. If not using docker please adjust accordingly

8. Run php artisan migrate —seed

9. If you go to http://127.0.0.1:8081 (port configured in the docker-compose file) you should be able to see the landing laravel page.

10. Now to fix the css on the login/register page you have to run npm install && npm run dev in nodejs container docker exec -it employee-app_nodejs_1 bash

11. Register a user to begin exploring the app.

To give a user admin rights, you can change a is_admin value to 1 in the users table.
Default is zero

For the email functionality I used https://mailtrap.io/. This can be swapped out in the .env if you have existing creds for any other email service that is compatible with Laravel.
